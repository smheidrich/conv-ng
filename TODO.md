# TODO

Next:


Soon:

[ ] Improve "look" of how marked-but-deleted files are represented in ls output
    (right now: just by their JSON containing `path_exists: False`)
[ ] Improve `ls` output to not just use dataclass repr inside each group

Later:

[ ] Consider making getting fresh data from FS not part of standard mark
    fetching but separate, e.g. via separate refresh command
[ ] ^ See also comment on fs-aware classes / manager method referencing that;
    do that, probably.
[ ] Consider putting unmarked paths in DB as well
[ ] Improve algorithm for getting fresh data from FS
[ ] Allow config file to specify root, then make all paths inside relative to
    that root
[ ] Ensure that the constraint of "only one explicit mark per path lineage
    allowed"
[ ] Clean up mark serialization (lots of holdovers from when there were
    hardcoded marks)
[ ] Make grouping by type lower level (move from ls CLI command to core), if
    that turns out to be reasonable (otherwise leave it as is)
[ ] Allow filtering by any type, not just `None`
[ ] Put filtering in core, not CLI
[ ] Consider much more user-defined filtering by outputting as JSON and letting
    user filter via JSONPath, then being able to format JSON for CLI output
