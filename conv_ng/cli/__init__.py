from . import commands
from .typer_app import cli_app


def main() -> None:
  cli_app()


__all__ = ["commands", "main"]

if __name__ == "__main__":
  main()
