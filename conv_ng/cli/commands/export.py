from os.path import abspath
from pathlib import Path
from typing import Optional

import typer

from ..typer_app import cli_app


@cli_app.command(
  help=(
    """
    Export all explicit marks under the given paths.
    """
  )
)
def export(
  ctx: typer.Context,
  paths: list[Path] = typer.Argument(
    None,
    help="Paths under which marks should be exported. "
    "Note that .. path components are resolved like in a shell "
    "(without resolving symlinks), not like in C.",
  ),
  type: Optional[str] = typer.Option(
    None, help="Mark type by which to filter."
  ),
) -> None:
  m = ctx.obj.mark_manager

  covered_paths = set()
  for path in paths:
    for mp in m.get_explicit_marks_below(Path(abspath(path))):
      if mp.path in covered_paths:
        continue
      # TODO Do this at a lower level
      if type is not None and mp.mark.json["type"] != type:
        continue
      covered_paths.add(mp.path)
      print(f"{mp.mark.json['type']}: {mp.path}")
