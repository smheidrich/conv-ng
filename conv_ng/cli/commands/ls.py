from collections import defaultdict
from os.path import abspath
from pathlib import Path
from textwrap import indent

import typer

from conv_ng.core.model import Mark, MarkInfo, MixedMark, SingleMark

from ..typer_app import cli_app

# TODO Consider doing this at a lower level too or moving as utility fn
# TODO   to core & adding tests.


def mark_info_or_none_as_display_str(mark_info: MarkInfo | None) -> str:
  if mark_info is None:
    return "None"
  return "\n".join(
    [
      mark_as_display_str(mark_info.mark),
      "because:",
      indent(str(mark_info.reason), "  "),
    ]
  )


def mark_as_display_str(mark: Mark | None) -> str:
  match mark:
    case SingleMark():
      return str(mark)
    case MixedMark(components=components):
      comp_strs_grouped_by_type = defaultdict(list)
      for c in components:
        if c is None:
          t = "None"
        elif isinstance(c, SingleMark):
          t = c.json["type"]
        else:
          # TODO Fix typing so this is ensured at type level: MixedMark can not
          # contain other MixedMarks
          raise RuntimeError("should never happen")
        comp_strs_grouped_by_type[t].append(mark_as_display_str(c))
      s = "\n".join(
        f"{t}:\n" + indent("\n".join(comp_strs), "  ")
        for t, comp_strs in comp_strs_grouped_by_type.items()
      )
      return "mixed:\n" + indent(s, "  ")
    case _:
      return str(mark)


# /TODO


@cli_app.command(
  help=(
    """
    List files along with their marks.
    """
  )
)
def ls(
  ctx: typer.Context,
  paths: list[Path] = typer.Argument(
    None,
    help="Paths to list files for. "
    "Note that .. path components are resolved like in a shell "
    "(without resolving symlinks), not like in C.",
  ),
  only_incomplete: bool = typer.Option(
    False,
    help="List only directories/files that haven't been marked yet "
    "(or not completely marked, in case of directories).",
  ),
) -> None:
  m = ctx.obj.mark_manager

  def _print_single_path_info(p: Path) -> None:
    marks = m.get_mark_info_for_path(Path(abspath(p)))
    if only_incomplete and not (
      marks is None
      or (isinstance(marks.mark, MixedMark) and None in marks.mark.components)
    ):
      return
    print(p)
    mark_info_str = mark_info_or_none_as_display_str(marks)
    print(indent(mark_info_str, "  "))

  if not paths:
    paths = [Path("")]
  paths = [Path(x) for x in paths]
  for i, path in enumerate(paths):
    if path.is_dir():
      if i > 0:
        print("")
      if len(paths) > 1:  # ls-like output for multiple dirs
        print(f"{path}/:")
      for p in sorted(m.iter_dir_fs_or_marked(path)):
        _print_single_path_info(p)
    else:
      _print_single_path_info(path)
