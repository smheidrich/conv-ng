import json
from os.path import abspath
from pathlib import Path

import typer

from conv_ng.core.model import single_mark_from_json

from ..typer_app import cli_app


@cli_app.command(
  help=(
    """
    Mark one or more files.
    """
  )
)
def mark(
  ctx: typer.Context,
  mark: str = typer.Argument(None, help="Mark to set (JSON)."),
  paths: list[Path] = typer.Argument(
    None,
    help="Paths to mark. "
    "Note that .. path components are resolved like in a shell "
    "(without resolving symlinks), not like in C.",
  ),
) -> None:
  m = ctx.obj.mark_manager

  try:
    mark_json_like = json.loads(mark)
    mark_obj = single_mark_from_json(mark_json_like)
  except Exception:
    raise ValueError(
      f"Could not decode JSON as mark: {mark!r}. "
      "The format is '{\"type\": MARK_TYPE, ... params ...}'."
      "See error messages above for more details."
    )

  for path in paths:
    m.set_mark_for_path(Path(abspath(path)), mark_obj)
