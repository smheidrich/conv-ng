from os.path import abspath
from pathlib import Path

import typer

from ..typer_app import cli_app


@cli_app.command(
  help=(
    """
    Unmark one or more files.
    """
  )
)
def unmark(
  ctx: typer.Context,
  paths: list[Path] = typer.Argument(
    None,
    help="Paths to unmark. "
    "Note that .. path components are resolved like in a shell "
    "(without resolving symlinks), not like in C.",
  ),
) -> None:
  m = ctx.obj.mark_manager

  for path in paths:
    m.set_mark_for_path(Path(abspath(path)), None)
