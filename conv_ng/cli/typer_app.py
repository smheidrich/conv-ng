from dataclasses import dataclass, field
from pathlib import Path

import typer

from conv_ng.core.manager import MarkManager
from conv_ng.core.persistence import MarkPersistence

from .. import __distribution_name__, __version__

cli_app = typer.Typer(
  context_settings={
    "auto_envvar_prefix": "CONV_NG",
  }
)


@dataclass
class TyperState:
  db_path: Path | None = None
  verbosity: int = 0

  mark_manager: MarkManager = field(init=False)

  def __post_init__(self) -> None:
    # TODO This should perhaps be moved into a separate App class or even into
    #   the Manager itself at some point...
    db_path = (
      p if (p := self.db_path) is not None else Path.home() / ".conv-ng.sqlite"
    )
    self.mark_manager = MarkManager(MarkPersistence(f"sqlite:///{db_path}"))


# Typer's idiom for implementing --version... don't even ask.
# https://typer.tiangolo.com/tutorial/options/version/
def version_callback(value: bool) -> None:
  if value:
    print(f"{__distribution_name__} {__version__}")
    raise typer.Exit()


@cli_app.callback(
  help=(
    """
    Convenient file selection for backups and more
    """
  )
)
def typer_callback(
  ctx: typer.Context,
  db_path: str = typer.Option(
    None, "--db", "-d", help="Path of the DB file for persistence"
  ),
  verbose: int = typer.Option(
    0,
    "--verbose",
    "-v",
    count=True,
    help="Verbose output (repeat to increase verbosity, e.g. -vv, -vvv).",
  ),
  version: bool = typer.Option(
    False,
    "--version",
    "-V",
    callback=version_callback,
    help="Print version information and exit.",
    is_eager=True,
  ),
) -> None:
  ctx.obj = TyperState(
    verbosity=verbose, db_path=Path(db_path) if db_path is not None else None
  )
