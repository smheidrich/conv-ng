from collections.abc import Iterable
from dataclasses import dataclass
from pathlib import Path

from .model import (
  ExplicitlyMarked,
  FsAwareMarkInfo,
  FsAwareMixedMark,
  FsAwareSingleMark,
  MarkedPath,
  MarkedViaChildren,
  MarkedViaParent,
  SingleMark,
  make_single_mark_fs_aware,
)
from .persistence import MarkPersistence, MarkPersistenceTransaction
from .utils import descendant_to_child, path_exists


@dataclass
class MarkManager:
  persistence: MarkPersistence

  # TODO: Eventually, split up into 1 fn that accesses FS and 1 that doesn't.
  #   See also comment on FsAwareMark* classes.
  def get_mark_info_for_path(self, path: Path) -> FsAwareMarkInfo | None:
    with self.persistence as tx:
      mark = tx.get_mark_for_path(path)
      marks_below = tx.get_set_of_marks_for_paths_below(path)
      marked_path_above = tx.get_first_marked_path_above(path)
    if mark is not None:  # explicit
      if marks_below:
        raise RuntimeError(
          f"bug: path {path} has {mark=}, but also {marks_below=}; "
          "having both set explicitly doesn't make sense"
        )
      if marked_path_above is not None:
        raise RuntimeError(
          f"bug: path {path} has {mark=}, but also {marked_path_above=}; "
          "having both set explicitly doesn't make sense"
        )
      return FsAwareMarkInfo(
        make_single_mark_fs_aware(
          mark, path_exists=path_exists(path, follow_symlinks=False)
        ),
        reason=ExplicitlyMarked(),
      )
    else:  # implicit
      if marks_below and marked_path_above is not None:
        raise RuntimeError(
          f"bug: path {path} has {marked_path_above=}, but also {marks_below=}; "
          "having both set explicitly doesn't make sense"
        )
      elif marks_below:
        # TODO The algorithm here is not very efficient and could be improved.
        # Find set of *FS-aware* marks below path by taking set of marks and
        # finding whether they refer to at least 1 path that exists and/or at
        # least 1 that doesn't (yes, in the worst cases, this goes through
        # either all marked files or all unmarked files):
        fs_aware_marks_below: set[FsAwareSingleMark | None] = set()
        for mark in marks_below:
          path_existences: set[bool] = set()
          for path_for_mark_below in tx.iter_paths_for_mark_below(mark):
            path_existences.add(
              path_exists(path_for_mark_below, follow_symlinks=False)
            )
            if len(path_existences) >= 2:
              break
          for path_existence in path_existences:
            fs_aware_marks_below.add(
              make_single_mark_fs_aware(mark, path_exists=path_existence)
            )
        # Next, figure out if there are also any unmarked files present:
        if self._has_unmarked_below_path(path, tx):
          fs_aware_marks_below.add(None)
        return FsAwareMarkInfo(
          FsAwareMixedMark(frozenset(fs_aware_marks_below)),
          reason=MarkedViaChildren(),
        )
      elif marked_path_above is not None:
        return FsAwareMarkInfo(
          make_single_mark_fs_aware(
            marked_path_above.mark,
            path_exists=path_exists(path, follow_symlinks=False),
          ),
          reason=MarkedViaParent(
            MarkedPath(marked_path_above.path, marked_path_above.mark)
          ),
        )
    return None

  def set_mark_for_path(self, path: Path, mark: SingleMark | None) -> None:
    with self.persistence as tx:
      marked_path_above = tx.get_first_marked_path_above(path)
      tx.delete_marks_for_paths_below(path)
      tx.delete_marks_for_paths_above(path)
      tx.delete_mark_for_path(path)
      if mark is not None:
        tx.update_mark_for_path(path, mark)
      # Apply previous parent mark to siblings instead:
      if marked_path_above is not None:
        # TODO Actually ensure this via type system:
        assert isinstance(marked_path_above.mark, SingleMark)
        for sibling in (p for p in path.parent.iterdir() if p != path):
          tx.update_mark_for_path(sibling, marked_path_above.mark)

  def get_explicit_marks_below(self, path: Path) -> list[MarkedPath]:
    with self.persistence as tx:
      return list(tx.iter_marked_paths_below(path))

  def iter_dir_fs_or_marked(self, path: Path) -> Iterable[Path]:
    """
    Iterate over dir entries, either present on the filesystem or marked.

    In other words, like `pathlib.Path.iterdir`, except it also includes
    files/directories inside `path` which don't exist on the filesystem but
    have marks attached to them.

    Args:
      path: Path of directory over which to iterate.

    Raises:
      NotADirectoryError: If `path` does not refer to a directory on the
        filesystem.
    """
    yielded = set()
    for child_path in path.iterdir():
      yield child_path
      yielded.add(child_path)
    with self.persistence as tx:
      # TODO This could be made much more efficient by excluding children that
      #   have already been yielded from the query: Currently, for /a, it will
      #   go through /a/b/c, /a/b/d, /a/b/e, ... for no reason; the first
      #   would've been enough to just yield /a/b and be done with that dir.
      absolute_path = path.absolute()  # DB only stores absolute paths
      for mark_info in tx.iter_marked_paths_below(absolute_path):
        descendant_path = mark_info.path  # not necessarily a direct child
        if not path.is_absolute():  # re-relativize path from DB if necessary
          descendant_path = descendant_path.relative_to(Path().absolute())
        child_path = descendant_to_child(path, descendant_path)
        if child_path in yielded:
          continue
        yield child_path

  def _has_unmarked_below_path(
    self, path: Path, tx: MarkPersistenceTransaction
  ) -> bool:
    if not path.is_dir():
      return False
    for subpath in path.iterdir():
      mark_at = tx.get_mark_for_path(subpath)
      if mark_at is not None:
        continue
      marks_below = tx.get_set_of_marks_for_paths_below(subpath)
      if not marks_below:
        return True
      if self._has_unmarked_below_path(subpath, tx):
        return True
    return False
