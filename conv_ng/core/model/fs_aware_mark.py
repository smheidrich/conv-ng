"""
FS-aware "marks", i.e. they contain information about whether the marked path
actually exists on the filesystem or not
"""
from dataclasses import dataclass
from typing import Any

from ..utils import frozendict
from .mark import JsonMark, Mark, MixedMark, SingleMark

# TODO: These are only derived from `Mark` classes to ensure
# backwards-compatibility with code using the outermost interface, but in a
# future backwards-compatibility-breaking version, this should probably be a
# single independent class instead which just contains a `Mark` instance as an
# attribute (the ones below already do, but it would be less hacky if they
# weren't also derived from `Mark` classes).


@dataclass(frozen=True)
class FsAwareMark(Mark):
  pass


@dataclass(frozen=True, kw_only=True)
class FsAwareSingleMark(FsAwareMark, SingleMark):
  fs_unaware_mark: SingleMark
  path_exists: bool

  def __init__(self, fs_unaware_mark: SingleMark, path_exists: bool):
    object.__setattr__(self, "fs_unaware_mark", fs_unaware_mark)
    object.__setattr__(self, "path_exists", path_exists)

  @property
  def json(self) -> dict[str, Any]:
    return {**super().json, "path_exists": self.path_exists}


@dataclass(frozen=True, kw_only=True)
class FsAwareJsonMark(FsAwareSingleMark, JsonMark):
  fs_unaware_mark: JsonMark

  def __init__(self, fs_unaware_mark: JsonMark, path_exists: bool):
    super().__init__(fs_unaware_mark, path_exists)

  @property  # type: ignore[misc]
  def type(self) -> str:
    return self.fs_unaware_mark.type

  @property  # type: ignore[misc]
  def data(self) -> frozendict[str, Any] | None:
    return self.fs_unaware_mark.data


def make_single_mark_fs_aware(
  fs_unaware_mark: SingleMark, path_exists: bool
) -> FsAwareSingleMark:
  """
  Builds the corresponding kind of FsAwareSingleMark.

  I.e., you give it a regular `SingleMark` and it gives you an
  `FsAwareSingleMark`, or you give it a `JsonMark` and it gives you an
  `FsAwareJsonMark`. This can't be easily expressed in Python type hints
  without HKTs or TypeScript-style utility types, so this natural-language
  description is the best you're going to get.
  """
  match fs_unaware_mark:
    case JsonMark():
      return FsAwareJsonMark(fs_unaware_mark, path_exists)
    case SingleMark():
      return FsAwareSingleMark(fs_unaware_mark, path_exists)
    case _:
      raise NotImplementedError(
        f"Don't know how to make mark {fs_unaware_mark} of type "
        f"{type(fs_unaware_mark)} fs-aware."
      )


#


@dataclass(frozen=True)
class FsAwareMixedMark(FsAwareMark, MixedMark):
  components: frozenset[FsAwareMark | None]
