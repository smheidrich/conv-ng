from dataclasses import dataclass

from .fs_aware_mark import FsAwareMark
from .mark_info import MarkInfo
from .mark_reason import MarkReason


@dataclass(frozen=True)
class FsAwareMarkInfo(MarkInfo):
  mark: FsAwareMark
  reason: MarkReason
