from dataclasses import dataclass
from typing import Any

from ..utils import frozendict


@dataclass(frozen=True)
class Mark:
  pass


@dataclass(frozen=True)
class SingleMark(Mark):
  @property
  def json(self) -> dict[str, Any]:
    return {"type": self.__class__.__name__}


@dataclass(frozen=True)
class JsonMark(SingleMark):
  type: str
  data: frozendict[str, Any] | None = None

  @property
  def json(self) -> dict[str, Any]:
    return {
      "type": self.type,
      **(
        {k: v for k, v in self.data.items()} if self.data is not None else {}
      ),
    }


#

# NOTE This is only temporary while we migrate from hardcoded to fully-dynamic
# mark types, so for now this can live among the models despite being an iface
# concern:
def single_mark_from_json(json_like: dict[str, Any]) -> SingleMark:
  type_str = json_like["type"]
  data = {k: v for k, v in json_like.items() if k != "type"}
  return JsonMark(type_str, data=frozendict(data) or None)


#


@dataclass(frozen=True)
class MixedMark(Mark):
  components: frozenset[Mark | None]
