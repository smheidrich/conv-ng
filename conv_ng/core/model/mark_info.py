from dataclasses import dataclass

from .mark import Mark
from .mark_reason import MarkReason


@dataclass(frozen=True)
class MarkInfo:
  mark: Mark
  reason: MarkReason
