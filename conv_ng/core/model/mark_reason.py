from dataclasses import dataclass

from .marked_path import MarkedPath


@dataclass(frozen=True)
class MarkReason:
  """
  Description of *why* a path has a certain mark.
  """


@dataclass(frozen=True)
class ExplicitlyMarked(MarkReason):
  """
  The mark was given explicitly for this specific path.
  """


@dataclass(frozen=True)
class MarkedViaParent(MarkReason):
  """
  The mark was propagated down from one of the path's parents.
  """

  parent_marked_path: MarkedPath


@dataclass(frozen=True)
class MarkedViaChildren(MarkReason):
  """
  The mark was propagated up from the path's children.

  These are not reproduced in the attributes like for MarkedViaParent because
  that can end up being quite a lot of data.
  """
