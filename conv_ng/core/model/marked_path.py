from dataclasses import dataclass
from pathlib import Path

from .mark import Mark, SingleMark


@dataclass(frozen=True)
class MarkedPath:
  path: Path
  mark: Mark


@dataclass(frozen=True)
class SingleMarkedPath(MarkedPath):
  mark: SingleMark
