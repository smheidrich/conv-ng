from dataclasses import dataclass
from pathlib import Path

from .mark_info import MarkInfo


@dataclass(frozen=True)
class MarkedPathInfo:
  path: Path
  mark_info: MarkInfo
