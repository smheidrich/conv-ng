from enum import Enum, auto


class Special(Enum):
  """
  Various special values (sometimes called "sentinel values").
  """

  DEFAULT = auto()
