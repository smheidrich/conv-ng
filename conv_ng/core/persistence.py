from collections.abc import Iterable
from contextlib import AbstractContextManager
from dataclasses import dataclass
from pathlib import Path
from typing import Any, cast

from canonicaljson import encode_canonical_json
from sqlalchemy import JSON, create_engine, delete, literal, select
from sqlalchemy.orm import DeclarativeBase, Mapped, Session, mapped_column

from .model import SingleMark
from .model import SingleMarkedPath as ModelSingleMarkedPath
from .model import single_mark_from_json
from .utils import ensure_trailing_path_sep


class Base(DeclarativeBase):
  pass


class MarkedPath(Base):
  __tablename__ = "marked_path"

  path: Mapped[str] = mapped_column(primary_key=True)
  mark_json: Mapped[JSON] = mapped_column(JSON, nullable=False)


@dataclass
class MarkPersistenceTransaction:
  session: Session

  def update_mark_for_path(self, path: Path, mark: SingleMark) -> None:
    self.session.add(
      MarkedPath(
        path=str(path),
        mark_json=mark.json,
      )
    )

  def delete_mark_for_path(self, path: Path) -> None:
    self.session.execute(
      delete(MarkedPath).where(MarkedPath.path == str(path))
    )

  def delete_marks_for_paths_below(self, path: Path) -> None:
    self.session.execute(
      delete(MarkedPath).where(MarkedPath.path.startswith(f"{path}/"))
    )

  def delete_marks_for_paths_above(self, path: Path) -> None:
    self.session.execute(
      delete(MarkedPath).where(
        # TODO What happens on the RHS when the path in the DB is the root /?
        #      Probably nothing good...
        literal(ensure_trailing_path_sep(path)).like(MarkedPath.path + "/%")
        & (MarkedPath.path != str(path))
      )
    )

  def get_mark_for_path(self, path: Path) -> SingleMark | None:
    mp = self.session.scalars(
      select(MarkedPath).where(MarkedPath.path == str(path))
    ).one_or_none()
    return self._make_single_mark(mp) if mp is not None else None

  def get_first_marked_path_above(
    self, path: Path
  ) -> ModelSingleMarkedPath | None:
    # TODO order by length desc
    mp = self.session.scalars(
      select(MarkedPath).where(
        # TODO What happens on the RHS when the path in the DB is the root /?
        #      Probably nothing good...
        literal(ensure_trailing_path_sep(path)).like(MarkedPath.path + "/%")
        & (MarkedPath.path != str(path))
      )
    ).one_or_none()
    return (
      ModelSingleMarkedPath(Path(mp.path), self._make_single_mark(mp))
      if mp is not None
      else None
    )

  def get_set_of_marks_for_paths_below(
    self, path: Path
  ) -> frozenset[SingleMark]:
    return frozenset(
      {
        self._make_single_mark(mp)
        for mp in self.session.scalars(
          select(MarkedPath).where(MarkedPath.path.like(str(path / "%")))
        )
      }
    )

  def iter_paths_for_mark_below(self, mark: SingleMark) -> Iterable[Path]:
    for mp in self.session.scalars(
      select(MarkedPath).where(MarkedPath.mark_json == mark.json)
    ):
      yield Path(mp.path)

  def iter_marked_paths_below(
    self, path: Path
  ) -> Iterable[ModelSingleMarkedPath]:
    for mp in self.session.scalars(
      select(MarkedPath).where(MarkedPath.path.like(str(path / "%")))
    ):
      yield ModelSingleMarkedPath(Path(mp.path), self._make_single_mark(mp))

  def _make_single_mark(self, marked_path: MarkedPath) -> SingleMark:
    return single_mark_from_json(cast(dict[str, Any], marked_path.mark_json))


@dataclass
class MarkPersistence(AbstractContextManager[MarkPersistenceTransaction]):
  db_url: str
  session: Session | None = None

  def __enter__(self) -> MarkPersistenceTransaction:
    # TODO echo=verbosity > x
    engine = create_engine(
      self.db_url,
      echo=False,
      # Important to preserve order etc. for comparisons:
      json_serializer=encode_canonical_json,
    )
    Base.metadata.create_all(engine)
    self.session = Session(engine)
    self.session.__enter__()
    return MarkPersistenceTransaction(self.session)

  def __exit__(self, *args: Any, **kwargs: Any) -> None:
    assert self.session is not None, "exiting non-entered context"
    if args == (None, None, None):  # commit if no errors
      self.session.commit()
    self.session.__exit__(*args, **kwargs)
