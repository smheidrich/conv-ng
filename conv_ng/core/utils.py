import os.path
from collections.abc import Iterable
from pathlib import Path
from typing import Any, Generic, TypeVar

# frozen dict

K = TypeVar("K")
V = TypeVar("V")


class frozendict(Generic[K, V]):
  def __init__(self, d: dict[K, V]) -> None:
    self._items = frozenset({(k, v) for k, v in d.items()})

  def items(self) -> Iterable[tuple[K, V]]:
    return self._items

  def __iter__(self) -> Iterable[K]:
    yield from (k for k, v in self._items)

  def __hash__(self) -> int:
    return self._items.__hash__()

  def __eq__(self, other: Any) -> bool:
    if not isinstance(other, self.__class__):
      return False
    return self._items.__eq__(other._items)

  def __repr__(self) -> str:
    inner = ", ".join(f"{k!r}: {v!r}" for k, v in self._items)
    return f"frozendict({{{inner}}})"

  def __bool__(self) -> bool:
    return bool(self._items)


# path stuff


def ensure_trailing_path_sep(path: Path) -> str:
  """
  Format a Path as a string so that it always ends in a separator (e.g. `/`).

  Equivalent to `f"{path}/` on POSIX, except when `path` is the root (`/`), in
  which case that would give `//` while this correctly returns `/`.
  """
  return os.path.join(path, "")


# TODO Get rid of this function when Python 3.12+ becomes the new minimum
# supported version, replace with `pathlib.Path.exists(follow_symlinks=...)
def path_exists(path: Path, *, follow_symlinks: bool) -> bool:
  """
  Check if something (file, dir, symlink, ...) exists at given path.

  Convenience function only needed while compatibility with Python < 3.12 is
  required; 3.12 introduces a `follow_symlinks` parameter to
  `pathlib.Path.exists`, making this unnecessary.
  """
  return path.is_symlink() or path.exists()


def descendant_to_child(parent: Path, descendant: Path) -> Path:
  """
  Turn an arbitrary-distance descendant into a direct child.

  E.g. for a parent `/foo` and descendant `/foo/bar/baz`, this returns
  `/foo/bar`. Works with absolute or relative paths.

  Raises:
    ValueError: If `descendant` is not a descendant of `parent` or if one path
      is relative and the other absolute (same exact exception as the one
      raised by `pathlib.Path.relative_to`).
  """
  return parent / descendant.relative_to(parent).parts[0]
