from pathlib import Path

import pytest

from conv_ng.core.persistence import MarkPersistence


@pytest.fixture()
def mark_persistence(tmp_path: Path) -> MarkPersistence:
  return MarkPersistence(f"sqlite:///{tmp_path}/db.sqlite")
