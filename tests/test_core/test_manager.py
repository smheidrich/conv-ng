import os
import os.path
from pathlib import Path

import pytest

from conv_ng.core.manager import MarkManager
from conv_ng.core.model import (
  ExplicitlyMarked,
  JsonMark,
  MarkedPath,
  MarkedViaChildren,
  MarkedViaParent,
)
from conv_ng.core.model.fs_aware_mark import FsAwareJsonMark, FsAwareMixedMark
from conv_ng.core.model.fs_aware_mark_info import FsAwareMarkInfo
from conv_ng.core.persistence import MarkPersistence
from conv_ng.core.utils import frozendict


@pytest.fixture()
def mark_manager(mark_persistence: MarkPersistence) -> MarkManager:
  return MarkManager(mark_persistence)


def test_get_simple_file_unmarked(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  path = tmp_path / "some-file"

  retrieved_mark = mark_manager.get_mark_info_for_path(path)

  assert retrieved_mark == None


def test_set_and_get_simple_file_explicit(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  path = tmp_path / "some-file"
  path.touch()
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))

  mark_manager.set_mark_for_path(path, mark)
  retrieved_mark = mark_manager.get_mark_info_for_path(path)

  assert retrieved_mark == FsAwareMarkInfo(
    mark=FsAwareJsonMark(mark, path_exists=True), reason=ExplicitlyMarked()
  )


def test_set_and_get_simple_file_implicit_via_parent(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  path = tmp_path / "some-file/below"
  path.parent.mkdir()
  path.touch()
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))

  mark_manager.set_mark_for_path(path.parent, mark)
  retrieved_mark = mark_manager.get_mark_info_for_path(path)

  assert retrieved_mark == FsAwareMarkInfo(
    mark=FsAwareJsonMark(mark, path_exists=True),
    reason=MarkedViaParent(MarkedPath(path.parent, mark)),
  )


def test_set_and_get_single_file_in_dir_parent_implicit_via_children(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  path = tmp_path / "some_dir/some-file"
  path.parent.mkdir()
  path.touch()
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))

  mark_manager.set_mark_for_path(path, mark)
  retrieved_mark = mark_manager.get_mark_info_for_path(path.parent)

  assert retrieved_mark == FsAwareMarkInfo(
    mark=FsAwareMixedMark(
      frozenset({FsAwareJsonMark(mark, path_exists=True)})
    ),
    reason=MarkedViaChildren(),
  )


def test_set_and_get_single_file_in_dir_parent_implicit_via_deleted_child(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  path = tmp_path / "some_dir/some-file"  # doesn't exist on FS! (simul. del.)
  path.parent.mkdir()
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))

  mark_manager.set_mark_for_path(path, mark)
  retrieved_mark = mark_manager.get_mark_info_for_path(path.parent)

  assert retrieved_mark == FsAwareMarkInfo(
    mark=FsAwareMixedMark(
      frozenset({FsAwareJsonMark(mark, path_exists=False)})
    ),
    reason=MarkedViaChildren(),
  )


def test_set_and_get_one_file_exists_one_deleted(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  parent = tmp_path / "some_dir"
  path1 = parent / "some-file"
  path2 = parent / "some-nonexistent"
  parent.mkdir()
  path1.touch()
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))

  mark_manager.set_mark_for_path(path1, mark)
  mark_manager.set_mark_for_path(path2, mark)
  retrieved_mark = mark_manager.get_mark_info_for_path(parent)

  assert retrieved_mark == FsAwareMarkInfo(
    mark=FsAwareMixedMark(
      frozenset(
        {
          FsAwareJsonMark(mark, path_exists=True),
          FsAwareJsonMark(mark, path_exists=False),
        }
      ),
    ),
    reason=MarkedViaChildren(),
  )


def test_set_and_get_single_link_in_dir_parent_implicit_via_children(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  """
  Regression test for when symlinks were resolved when they shouldn't be.
  """
  path = tmp_path / "some_dir/some-link"
  path.parent.mkdir()
  path.symlink_to("/doesntmatter")
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))

  mark_manager.set_mark_for_path(path, mark)
  retrieved_mark = mark_manager.get_mark_info_for_path(path.parent)

  assert retrieved_mark == FsAwareMarkInfo(
    mark=FsAwareMixedMark(
      frozenset({FsAwareJsonMark(mark, path_exists=True)})
    ),
    reason=MarkedViaChildren(),
  )


def test_set_and_get_file_in_dir_some_unset_parent_implicit_via_children(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  path = tmp_path / "some_dir/some-file"
  path_unmarked = path.parent / "some-unmarked-file"
  path.parent.mkdir()
  path.touch()
  path_unmarked.touch()
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))

  mark_manager.set_mark_for_path(path, mark)
  retrieved_mark = mark_manager.get_mark_info_for_path(path.parent)

  assert retrieved_mark == FsAwareMarkInfo(
    mark=FsAwareMixedMark(
      frozenset({FsAwareJsonMark(mark, path_exists=True), None})
    ),
    reason=MarkedViaChildren(),
  )


def test_set_mark_below_existing(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  # prepare
  initially_marked_parent_path = tmp_path / "some"
  initially_marked_parent_path.mkdir()
  initial_parent_mark = JsonMark("ignore")
  mark_manager.set_mark_for_path(
    initially_marked_parent_path, initial_parent_mark
  )
  child_to_be_re_marked_path = (
    initially_marked_parent_path / "child-to-be-re-marked"
  )
  child_to_be_re_marked_path.touch()
  sibling_path = initially_marked_parent_path / "sibling"
  sibling_path.touch()
  # run
  new_child_mark = JsonMark(
    "backup", frozendict({"destination": "some-archive"})
  )
  mark_manager.set_mark_for_path(child_to_be_re_marked_path, new_child_mark)
  # check
  parent_mark_info_after = mark_manager.get_mark_info_for_path(
    initially_marked_parent_path
  )
  assert parent_mark_info_after == FsAwareMarkInfo(
    mark=FsAwareMixedMark(
      frozenset(
        {
          FsAwareJsonMark(initial_parent_mark, path_exists=True),
          FsAwareJsonMark(new_child_mark, path_exists=True),
        }
      )
    ),
    reason=MarkedViaChildren(),
  )
  re_marked_child_mark_info_after = mark_manager.get_mark_info_for_path(
    child_to_be_re_marked_path
  )
  assert re_marked_child_mark_info_after == FsAwareMarkInfo(
    FsAwareJsonMark(new_child_mark, path_exists=True), ExplicitlyMarked()
  )
  sibling_mark_after = mark_manager.get_mark_info_for_path(sibling_path)
  assert sibling_mark_after == FsAwareMarkInfo(
    FsAwareJsonMark(initial_parent_mark, path_exists=True),
    reason=ExplicitlyMarked(),
  )


def test_iter_dir_fs_or_marked(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  # prepare: set up paths
  dir_path = tmp_path / "some"
  existent_unmarked_path = dir_path / "existent-unmarked"
  existent_marked_path = dir_path / "existent-marked"
  existent_marked_path_with_children = dir_path / "existent-marked-w-children"
  nonexistent_marked_path = dir_path / "nonexistent-marked"
  nonexistent_unmarked_path_with_marked_desc = (
    dir_path / "nonexistent-marked-desc"
  )
  marked_descendant_path = (
    nonexistent_unmarked_path_with_marked_desc / "desc-marked"
  )
  # prepare: create dirs and files
  dir_path.mkdir()
  existent_unmarked_path.touch()
  existent_marked_path.touch()
  existent_marked_path_with_children.mkdir()
  (existent_marked_path_with_children / "child").touch()
  # prepare: create marks
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))
  mark_manager.set_mark_for_path(existent_marked_path, mark)
  mark_manager.set_mark_for_path(existent_marked_path_with_children, mark)
  mark_manager.set_mark_for_path(nonexistent_marked_path, mark)
  mark_manager.set_mark_for_path(marked_descendant_path, mark)
  # run
  paths = list(mark_manager.iter_dir_fs_or_marked(dir_path))
  # check: contains all paths we expect and none we don't
  assert set(paths) == {
    existent_unmarked_path,
    existent_marked_path,
    existent_marked_path_with_children,
    nonexistent_marked_path,
    nonexistent_unmarked_path_with_marked_desc,
  }
  # check: no duplicates
  duplicates = {p: c for p in paths if (c := paths.count(p)) > 1}
  assert duplicates == {}


def test_iter_dir_fs_or_marked_relative_cwd(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  """
  Regression test for bug in `ls .` CLI invocation.
  """
  # prepare: set up paths
  dir_path = tmp_path / "some"
  marked_path = dir_path / "existent-marked"
  # prepare: create dirs and files
  dir_path.mkdir()
  marked_path.touch()
  # prepare: create marks
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))
  mark_manager.set_mark_for_path(marked_path, mark)
  # run
  oldcwd = os.getcwd()
  os.chdir(dir_path)
  try:
    paths = list(mark_manager.iter_dir_fs_or_marked(Path()))
  finally:
    os.chdir(oldcwd)
  # check: contains all paths we expect and none we don't
  assert set(paths) == {Path("existent-marked")}
  # check: no duplicates
  duplicates = {p: c for p in paths if (c := paths.count(p)) > 1}
  assert duplicates == {}


def test_iter_dir_fs_or_marked_relative_cwd_child(
  mark_manager: MarkManager, tmp_path: Path
) -> None:
  """
  Regression test for bug in `ls ./dir` CLI invocation.
  """
  # prepare: set up paths
  dir_parent_path = tmp_path / "some"
  dir_path = dir_parent_path / "dir"
  marked_path = dir_path / "existent-marked"
  # prepare: create dirs and files
  dir_path.mkdir(parents=True)
  marked_path.touch()
  # prepare: create marks
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))
  mark_manager.set_mark_for_path(marked_path, mark)
  # run
  oldcwd = os.getcwd()
  os.chdir(dir_parent_path)
  try:
    paths = list(mark_manager.iter_dir_fs_or_marked(Path("dir")))
  finally:
    os.chdir(oldcwd)
  # check: contains all paths we expect and none we don't
  assert set(paths) == {Path("dir/existent-marked")}
  # check: no duplicates
  duplicates = {p: c for p in paths if (c := paths.count(p)) > 1}
  assert duplicates == {}
