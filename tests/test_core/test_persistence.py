from pathlib import Path

import pytest

from conv_ng.core.model import JsonMark
from conv_ng.core.model import SingleMarkedPath as ModelSingleMarkedPath
from conv_ng.core.persistence import MarkPersistence
from conv_ng.core.utils import frozendict


def test_add_and_read_simple(mark_persistence: MarkPersistence) -> None:
  path = Path("/some/path")
  mark = JsonMark("backup", frozendict({"destination": "some-dest"}))
  with mark_persistence as tx:
    tx.update_mark_for_path(path, mark)
  with mark_persistence as tx:
    assert tx.get_mark_for_path(path) == mark
    assert tx.get_first_marked_path_above(path) is None
    assert tx.get_first_marked_path_above(
      path / "below"
    ) == ModelSingleMarkedPath(path, mark)
    assert tx.get_set_of_marks_for_paths_below(path) == set()
    assert tx.get_set_of_marks_for_paths_below(path.parent) == {mark}


def test_add_and_read_common_prefix(mark_persistence: MarkPersistence) -> None:
  path1 = Path("/some/path")
  path2 = Path("/some/path2")
  mark1 = JsonMark("backup", frozendict({"destination": "some-dest"}))
  mark2 = JsonMark("ignore")
  with mark_persistence as tx:
    tx.update_mark_for_path(path1, mark1)
    tx.update_mark_for_path(path2, mark2)
  with mark_persistence as tx:
    assert tx.get_mark_for_path(path1) == mark1
    assert tx.get_first_marked_path_above(path1) is None
    assert tx.get_first_marked_path_above(
      path1 / "below"
    ) == ModelSingleMarkedPath(path1, mark1)
    assert tx.get_set_of_marks_for_paths_below(path1) == set()
    assert tx.get_mark_for_path(path2) == mark2
    assert tx.get_first_marked_path_above(path2) is None
    assert tx.get_first_marked_path_above(
      path2 / "below"
    ) == ModelSingleMarkedPath(path2, mark2)
    assert tx.get_set_of_marks_for_paths_below(path2) == set()
    # This is above both of them, of course:
    assert tx.get_set_of_marks_for_paths_below(path1.parent) == {mark1, mark2}


def test_delete(mark_persistence: MarkPersistence) -> None:
  path = Path("/some/path")
  with mark_persistence as tx:
    tx.update_mark_for_path(
      path, JsonMark("backup", frozendict({"destination": "some-dest"}))
    )
  with mark_persistence as tx:
    tx.delete_mark_for_path(path)
  with mark_persistence as tx:
    assert tx.get_mark_for_path(path) is None
    assert tx.get_first_marked_path_above(path) is None
    assert tx.get_set_of_marks_for_paths_below(path) == set()


def test_delete_below_simple(mark_persistence: MarkPersistence) -> None:
  path1 = Path("/some/path")
  path2 = Path("/some/path2")
  mark1 = JsonMark("backup", frozendict({"destination": "some-dest"}))
  mark2 = JsonMark("ignore")
  with mark_persistence as tx:
    tx.update_mark_for_path(path1, mark1)
    tx.update_mark_for_path(path2, mark2)
  with mark_persistence as tx:
    tx.delete_marks_for_paths_below(path1.parent)
  with mark_persistence as tx:
    assert tx.get_mark_for_path(path1) is None
    assert tx.get_mark_for_path(path2) is None


def test_delete_below_common_prefix(mark_persistence: MarkPersistence) -> None:
  path1 = Path("/some/path")
  path2 = Path("/some1/path")
  mark1 = JsonMark("backup", frozendict({"destination": "some-dest"}))
  mark2 = JsonMark("ignore")
  with mark_persistence as tx:
    tx.update_mark_for_path(path1, mark1)
    tx.update_mark_for_path(path2, mark2)
  with mark_persistence as tx:
    tx.delete_marks_for_paths_below(path1.parent)
  with mark_persistence as tx:
    assert tx.get_mark_for_path(path1) is None
    assert tx.get_mark_for_path(path2) == mark2


def test_delete_above_simple(mark_persistence: MarkPersistence) -> None:
  path1 = Path("/some/path")
  mark1 = JsonMark("backup", frozendict({"destination": "some-dest"}))
  with mark_persistence as tx:
    tx.update_mark_for_path(path1, mark1)
    tx.update_mark_for_path(path1 / "below", mark1)
  with mark_persistence as tx:
    tx.delete_marks_for_paths_above(path1 / "below")
  with mark_persistence as tx:
    assert tx.get_mark_for_path(path1) is None
    assert tx.get_mark_for_path(path1 / "below") == mark1


def test_delete_above_common_prefix(mark_persistence: MarkPersistence) -> None:
  path1 = Path("/some/path")
  path2 = Path("/some/path2")
  mark1 = JsonMark("backup", frozendict({"destination": "some-dest"}))
  mark2 = JsonMark("ignore")
  with mark_persistence as tx:
    tx.update_mark_for_path(path1, mark1)
    tx.update_mark_for_path(path1 / "below", mark1)
    tx.update_mark_for_path(path2, mark2)
    tx.update_mark_for_path(path2 / "below", mark2)
  with mark_persistence as tx:
    tx.delete_marks_for_paths_above(path2 / "below")
  with mark_persistence as tx:
    assert tx.get_mark_for_path(path1) == mark1
    assert tx.get_mark_for_path(path1 / "below") == mark1
    assert tx.get_mark_for_path(path2) is None
    assert tx.get_mark_for_path(path2 / "below") == mark2


def test_iter_paths_for_mark_below(mark_persistence: MarkPersistence) -> None:
  path = Path("/some/path")
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))
  with mark_persistence as tx:
    tx.update_mark_for_path(path / "below", mark)
  with mark_persistence as tx:
    l = list(tx.iter_paths_for_mark_below(mark))
  assert len(l) == 1
  assert l[0] == path / "below"


@pytest.mark.parametrize("query_path", ["/some", "/"])
def test_iter_marked_paths_below(
  mark_persistence: MarkPersistence, query_path: str
) -> None:
  # Setup
  path = Path("/some/path")
  mark = JsonMark("backup", frozendict({"destination": "some-archive"}))
  # Run
  with mark_persistence as tx:
    tx.update_mark_for_path(path, mark)
  # Sanity check: Querying paths below the marked path yields nothing
  with mark_persistence as tx:
    l = list(tx.iter_marked_paths_below(path / "foo"))
  assert len(l) == 0
  # Check:
  with mark_persistence as tx:
    l = list(tx.iter_marked_paths_below(Path(query_path)))
  assert len(l) == 1
  assert l[0] == ModelSingleMarkedPath(path, mark)


@pytest.mark.parametrize("query_path", ["/some", "/"])
def test_get_set_of_marks_for_paths_below(
  mark_persistence: MarkPersistence, query_path: str
) -> None:
  # Setup
  path1 = Path("/some/path")
  path2 = Path("/some/path2")
  mark1 = JsonMark("backup", frozendict({"destination": "some-archive"}))
  mark2 = JsonMark("ignore")
  # Run
  with mark_persistence as tx:
    tx.update_mark_for_path(path1, mark1)
    tx.update_mark_for_path(path2, mark2)
  # Sanity check: Querying paths below the marked paths yields nothing
  with mark_persistence as tx:
    s1 = tx.get_set_of_marks_for_paths_below(path1 / "foo")
    s2 = tx.get_set_of_marks_for_paths_below(path2 / "foo")
    s = s1 | s2
  assert s == set()
  # Sanity check: Querying completely unrelated paths yields nothing
  with mark_persistence as tx:
    assert tx.get_set_of_marks_for_paths_below(Path("/other")) == set()
  # Check:
  with mark_persistence as tx:
    s = tx.get_set_of_marks_for_paths_below(Path(query_path))
  assert s == {mark1, mark2}
